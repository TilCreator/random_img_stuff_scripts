#!/usr/bin/env python3

# Usage: ./telegram_export_group.py chat_id directory [> links]

from telethon.sync import TelegramClient
from telethon.tl.functions.messages import GetAllStickersRequest, GetArchivedStickersRequest, GetStickerSetRequest
from telethon.tl.types import InputStickerSetID
from telethon.utils import guess_extension
import logging
import os
import sys
import glob

import api_keys


chat_id = sys.argv[1]
directory = sys.argv[2]


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

if not os.path.exists(directory):
    os.mkdir(directory)

last_msg_id = max([0] + [int(file[:file.find("-")]) for file in os.listdir(directory) if not file.startswith("_")])

print(last_msg_id)

with TelegramClient(api_keys.telegram_name, api_keys.telegram_api_id, api_keys.telegram_api_hash) as client:
    os.chdir(directory)
    for i, message in enumerate(client.iter_messages(chat_id, reverse=True, offset_id=last_msg_id-1)):
        file_name = f'{message.id}-{chat_id}-{message.date.isoformat()}'
        print(message.date, '=>', file_name, file=sys.stderr)
        for unfinished_file in glob.glob('_' + glob.escape(file_name) + '*'):
            print(unfinished_file, file=sys.stderr)
            os.remove(unfinished_file)
        download_file_name = message.download_media(file='_' + file_name)
        if download_file_name is not None:
            download_file_name = os.path.basename(download_file_name)
            os.rename(download_file_name, download_file_name[1::])
        if message.text is not None:
            sanitized_text = message.text.replace('"', '')
            print(f'{chat_id}; {message.date.isoformat()}; "{sanitized_text}"')

