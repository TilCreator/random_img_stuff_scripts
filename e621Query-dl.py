#!/usr/bin/env python3

# Usage: ./e621Query-dl.py query root_folder

import e621
import api_keys
import requests
from sys import argv
from os import mkdir, path

e = e621.E621(bot_name='tc_does_stuff', version='0.0', user_nick='TilCreator', api_key=api_keys.e621)

query_str = argv[1]
posts = e.posts(query_str)
name = query_str.replace('/', '-')

print(f'Catched: "{query_str}" ({len(posts)} posts)')

try:
    mkdir(path.join(argv[2], name))
except FileExistsError:
    pass

for i, post in enumerate(posts):
    file_name = f'{post["id"]}-{post["file"]["md5"]}.{post["file"]["ext"]}'

    if not path.exists(path.join(argv[2], name, file_name)):
        open(path.join(argv[2], name, file_name), 'wb').write(requests.get(post['file']['url']).content)

    print(f'Downloaded: "{file_name}" ({i}/{len(posts)})                    ', end='\r')
print(f'Downloaded: "{query_str}" ({len(posts)} posts)                    ')
