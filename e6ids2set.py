#!/usr/bin/env python3

# Usage: cat e6ids | ./e6ids2set.py set_id
# Caches actions and doesn't repeat any!

from e621 import E621
import sys
import os

import api_keys


def line_in_file(f, query):
    f.seek(0)
    for line in f.readlines():
        if line.strip('\n') == query:
            return True
    return False


e = E621('yiff collection syncer', version='0.0', user_nick='TilCreator', api_key=api_keys.e621)

with open(f'_scripts/state/e6ids_set_add_{str(int(sys.argv[1]))}', 'a+') as ids:
    for line in sys.stdin:
        if os.environ.get("SHELL") is not None:
            print(int(line), end='\r')

        if line_in_file(ids, str(int(line))):
            continue

        r = e.set_add_post(set_id=int(sys.argv[1]), post_id=int(line))

        print(int(line), str(r).strip('\n'))

        if r is None or r['success'] is True or 'already exists in the set' in r['reason'] or 'You cannot add deleted posts to sets' == r['reason']:
            ids.seek(0, 2)
            ids.write(str(int(line)) + '\n')
            ids.flush()
