#!/usr/bin/sh
find . -type f -name '*' -exec file {} \; | grep -o -P '^.+: \w+ image' | cut -d':' -f1 | find -printf "%T@|%p\n" | sort -n | cut -d'|' -f2
