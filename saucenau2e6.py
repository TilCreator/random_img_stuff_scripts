#!/usr/bin/env python3

# Usage: cat paths | ./saucenao2e6.py [<restart tor>]
# Writes (/reads) _scripts/state/[e6ids|none6_paths_done] /tmp/saucenao_log

import sys
import os
import re
import time
import json
import logging
import saucenao


def line_in_file(f, query, ignore_score=False):
    f.seek(0)
    for line in f.readlines():
        if ignore_score:
            line = line[line.find(':') + 1:]
        if line.strip('\n') == query:
            return True
    return False


logger = logging.getLogger('saucenao2e6')
if os.environ.get("SHELL") is not None or False:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.ERROR)

max_retries = 0
restart_tor = len(sys.argv) > 1 and bool(sys.argv[1])

use_api_keys = False
api_keys = [
    '90223b33293be57f8f458bbb3c5ee37438153777',
    'c9506f322859c57eb4b530d181d644c760b2191c',
    '25245b0ea3573dabccbb94d18a65f3bc23d58bac',
    '7c61c33b1c80ac45f3738929939e4672051a4e9d',
    '0b6500b7021ae2485c5b28c371e95146d196628a',
    'ffd4bac7cc266b651a461f8149ef215ded590f9a',
    '8770d6433cd996a74841cea4fcb9a3103d832056'
]
api_keys_i = 0

if use_api_keys:
    s = saucenao.SauceNao(directory='', api_key=api_keys[api_keys_i], log_level=logger.level, databases=29)
else:
    s = saucenao.SauceNao(directory='', log_level=logger.level, databases=29)

with open('_scripts/state/saucenao_results', 'a+') as saucenao_results:
    with open('_scripts/state/e6ids', 'a+') as e6ids:
        with open('_scripts/state/none6_paths_done', 'a+') as none6_paths_done:
            for line in sys.stdin:
                if os.environ.get("SHELL") is not None:
                    print(line.strip('\n'), end='\r')

                if not line_in_file(none6_paths_done, line.strip('\n'), True) and line.strip('\n')[line.strip('\n').rfind('.') + 1:] in ['gif', 'jpg', 'jpeg', 'png', 'bmp', 'svg', 'webp']:
                    if not os.path.exists(line.strip('\n')):
                        logger.warning('FileNotFoundError: ' + line.strip('\n'))
                        continue

                    retry_counter = 0
                    while True:
                        try:
                            results = s.check_file(line.strip('\n'))
                        except Exception as e:  # (saucenao.exceptions.DailyLimitReachedException, saucenao.exceptions.UnknownStatusCodeException) as e:
                            if use_api_keys:
                                api_keys_i = (api_keys_i + 1) % len(api_keys)

                                s.api_key = api_keys[api_keys_i]

                                logger.warning('apikeys cyceling ' + str(api_keys_i) + ' ' + s.api_key + ' ' + str(e) + ' ' + str(s.previous_status_code))

                            logger.warning(e)

                            if retry_counter > max_retries or max_retries == -1:
                                if restart_tor:
                                    print('Restarting tor')
                                    os.system('/usr/bin/systemctl restart tor.service')
                                    retry_counter = 0
                                else:
                                    raise RuntimeError('max retries excided')

                            retry_counter += 1

                            time.sleep(7)
                        else:
                            break

                    score = 0

                    out_print = line.strip('\n') + ' '

                    saucenao_results.write(json.dumps({line.strip('\n'): results}) + '\n')
                    saucenao_results.flush()

                    if len(results) > 0:
                        for result in results:
                            for link in result['data']['ext_urls']:
                                if re.match(r'https://e621.net/post/show/([0-9]*)', link):
                                    e6id = str(link[link.rfind('/') + 1:])

                                    score_tmp = float(result['header']['similarity']) / 100
                                    if score_tmp > score:
                                        score = score_tmp

                                    if not line_in_file(e6ids, e6id):
                                        e6ids.seek(0, 2)
                                        e6ids.write(e6id + '\n')
                                        e6ids.flush()

                                    out_print += e6id + ' '

                    print(out_print)

                    none6_paths_done.seek(0, 2)
                    none6_paths_done.write(str(score) + ':' + line.strip('\n') + '\n')
                    none6_paths_done.flush()
