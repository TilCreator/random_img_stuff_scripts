from requests_toolbelt.multipart.encoder import MultipartEncoder
from typing import NoReturn, Dict, List, Any, Generator, Union
from io import IOBase
import requests
import hashlib
import logging
import time


logger = logging.getLogger(__name__)


MIN_DELAY_TIME = 0.8
TIMEOUT = 5
TIMEOUT_UPLOAD = 20
RETRY_COUNT = 3
MAX_POSTS_LIMIT = 320


class E621():
    def __init__(self, bot_name:str, user_nick:str, api_key:str=None, version:str='0.0') -> NoReturn:
        self.api_key = api_key
        self.name = bot_name
        self.version = version
        self.nick = user_nick

        self.access_time = time.time() - MIN_DELAY_TIME

    def _make_request(self, methode:str, path:str, auth:bool=True, files:Union[dict, None]=None, **args:Dict[str, str]) -> Dict[str, Any]:
        if auth and self.api_key:
            args['login'] = self.nick
            args['api_key'] = self.api_key

        logger.debug(f'Requesting "https://e621.net/{path}.json", args: "{args}", User-Agent: "{self.name}/{self.version} (by {self.nick} on e621)"')

        retry_count = 0
        while retry_count <= RETRY_COUNT:
            while (self.access_time - time.time()) + MIN_DELAY_TIME > 0:
                time.sleep(max(0, (self.access_time - time.time()) + MIN_DELAY_TIME))
            self.access_time = time.time()

            try:
                r = requests.request(methode, f'https://e621.net/{path}.json', files=files, data=args,
                                     headers={'User-Agent': f'{self.name}/{self.version} (by {self.nick} on e621)'}, timeout=TIMEOUT)
            except Exception as e:
                retry_count += 1

                if retry_count <= RETRY_COUNT:
                    continue

                raise e

            logger.debug(f'Received text:\n{r.text}')

            return r.json()

    def posts_generator(self, tags:Union[str, List[str]], limit:Union[int, None]=None, page:Union[int, None]=None,
                        before:Union[int, None]=None, after:Union[int, None]=None, absolute_paging:bool=False) -> Generator:
        if type(tags) is list:
            tags = ' '.join(tags)

        assert len([True for paging_param in [page, after, before] if paging_param is not None]) <= 1, 'The page, before and after parameter are not compatable with each other'

        if before is not None:
            page = f'b{before}'
        elif after is not None:
            page = f'a{after}'

        request_limit = 320

        while True:
            if limit is not None:
                if limit <= 0:
                    break

                request_limit = min(MAX_POSTS_LIMIT, limit)

            posts = self._make_request('GET', 'posts', tags=tags, limit=request_limit, page=page, auth=True)['posts']

            if len(posts) <= 0:
                break

            if limit is not None:
                limit -= len(posts)

            if absolute_paging:
                page += 1
            else:
                page = f'b{posts[-1]["id"]}'

            for post in posts:

                yield post

    def posts(self, tags:Union[str, List[str]], limit:Union[int, None]=None, page:Union[int, None]=None,
              before:Union[int, None]=None, after:Union[int, None]=None, absolute_paging:bool=False) -> List[dict]:
        return list(self.posts_generator(tags, limit, page, before, after, absolute_paging))

    def upload_post(self, tag_string:Union[List[str], str], rating:str, file:Union[str, IOBase]=None, direct_url:Union[None, str]=None,
                    source:str='', description:str='', parent_id:Union[int, None]=None, md5_confirmation:str=None, as_pending:str='') -> Dict[str, Any]:
        assert bool(file is not None) ^ bool(direct_url is not None), 'Only source url or direct url can be specified'

        if type(tag_string) is list:
            tag_string = ' '.join(tag_string)

        files = None

        if file is not None:
            if type(file) is str:
                file = open(file, 'rb')

            if md5_confirmation is None:
                hash_md5 = hashlib.md5()  # https://stackoverflow.com/a/3431838
                for chunk in iter(lambda: file.read(4096), b""):
                    hash_md5.update(chunk)
                md5_confirmation = hash_md5.hexdigest()
                file.seek(0)

            files = {'upload[file]': (None, file)}

        return self._make_request('POST', 'uploads', auth=True, files=files,
                                  **{'upload[tag_string]': tag_string, 'upload[rating]': rating,
                                     'upload[direct_url]': direct_url, 'upload[source': source,
                                     'upload[description': description, 'upload[parent_id': parent_id})
                                     #, 'upload[md5_confirmation': md5_confirmation})

    def post(self, id:int) -> Dict[str, Any]:  # GET posts/id
        return self._make_request('GET', f'posts/{id}', id=id)['post']

    def post_patch(self):  # PATCH posts/id
        pass

    def flags_generator(self):  # GET flags
        pass

    def flags(self):  # GET flags
        pass

    def flags_post(self):  # POST flags
        pass

    def vote(self, id:int, score:int, no_unvote:bool=True) -> Dict[str, Any]:  # POST posts/id/votes
        assert score == -1 or score == 1, 'score has to be 1 or -1'

        return self._make_request('POST', f'posts/{id}/votes', id=id, auth=True,
                                  no_unvote="true" if no_unvote else "false",
                                  score=score)

    def notes_generator(self):  # GET notes
        pass

    def notes(self):  # GET notes
        pass

    def notes_post(self):  # POST notes
        pass

    def note(self):  # GET notes/id
        pass

    def note_post(self):  # POST notes/id
        pass

    def note_update(self):  # PUT notes/id
        pass

    def note_delete(self):  # DELETE notes/id
        pass

    def note_revert(self):  # PUT notes/id/revert
        pass

    def pools_generator(self):  # GET pools
        pass

    def pools(self):  # GET pools
        pass

    def pool(self, pool_id:int):  # GET pools/id
        return self._make_request('GET', f'pools/{pool_id}')

    def pool_post(self):  # POST pools
        pass

    def pool_update(self):  # PUT pools/id
        pass

    def pool_revert(self):  # PUT pools/id/revert
        pass

    def favorite(self):  # TODO
        pass

    # File needs to be a 'rb' file object
    def iqdb(self, file=None, url:Union[str, None]=None, post_id:Union[id, None]=None) -> List[dict]:
        assert bool(file is not None) ^ bool(url is not None) ^ bool(post_id is not None) , 'Only file, url or post_id can be specified at once'

        if not post_id is None:
            return self._make_request('get', 'iqdb_queries', auth=True, post_id=str(post_id))
        elif not url is None:
            return self._make_request('post', 'iqdb_queries', auth=True, url=url)
        elif not file is None:
            return self._make_request('post', 'iqdb_queries', auth=True, files={"file": file})
