#!/usr/bin/env python3

# Usage: ./list-dl path '["link.img", ...]''

from sys import argv
from urllib.request import urlretrieve
import os
import json

path = argv[1]
imgs = json.loads(argv[2])

print(f'Path: {path}, list_len: {len(imgs)}')

try:
    os.mkdir(path)
except FileExistsError:
    pass

os.chdir(path)

for i, img in enumerate(imgs):
    name = f'{i:04}{img[img.rfind("."):]}'

    urlretrieve(img, name)

    print(f'Downloaded: "{name}" ({i}/{len(imgs)})', end='\r')

