import praw
from urllib.request import urlretrieve
from urllib.error import HTTPError
from sys import argv
from os.path import join

reddit = praw.Reddit(client_id='K1sHDaTCkKZYuQ', client_secret='VzlJwGUHiguf5Q0Gtd990aj4Tsk', user_agent='somebot')

if len(argv) < 4:
    print(f'{argv[0]} <subredditname> <limit> <folderpath>')
    exit()

for item in reddit.subreddit(argv[1]).top(limit=int(argv[2])):
    if item.url[item.url.rfind('/')] == '':
        continue

    try:
        urlretrieve(item.url, join(argv[3], item.url[item.url.rfind('/') + 1:]))
    except HTTPError:
        continue

    print(item.url, '>', join(argv[3], item.url[item.url.rfind('/') + 1:]))
