#!/usr/bin/env python3

# Usage: ./e6set2ids.py set_id > output

from e621 import E621
import sys

import api_keys


e = E621('yiff collection syncer', version='0.0', user_nick='TilCreator', api_key=api_keys.e621)

r = e.set_show(id=int(sys.argv[1]))

print('\n'.join([str(post) for post in r['posts']]))
