#!/usr/bin/sh

# Usage:
# ./get_random_artist.sh </path/to/notes.md>
# Gets a random artist from the notes.md file

awk '1;/^$/{exit}' $1 | grep '^- ' | cut -d' ' -f2 | sort -R | head -n3
