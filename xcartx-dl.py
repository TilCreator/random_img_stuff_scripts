from urllib.request import urlretrieve, build_opener, install_opener
from pyquery import PyQuery as pq
from os import mkdir, chdir
from sys import argv
import re

opener = build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0')]
install_opener(opener)

name = re.search(r'[0-9]+-[a-zA-Z0-9\-]+', argv[1])[0]  # input("name: ")
url = f'https://xcartx.com/{name}.html'

s = pq(url=url)

dir = s('h1.post-title').text()
print(dir)
try:
    mkdir(dir)
except FileExistsError:
    pass
chdir(dir)

for i, img in enumerate(s('#dle-content table img')):
    imgUrl = pq(img).attr('src')

    if 'uploads' in imgUrl:
        ext = imgUrl[imgUrl.rfind('.'):]

        if imgUrl[:1] == '/':
            imgUrl = f'https://xcartx.com{imgUrl}'

        urlretrieve(imgUrl, f'{i:03}{ext}')

        print(f'{imgUrl} > {i:03}{ext}')
