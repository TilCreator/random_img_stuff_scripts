from urllib.request import urlretrieve, build_opener, install_opener
from pyquery import PyQuery as pq
from os import mkdir, chdir
from sys import argv

opener = build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0')]
install_opener(opener)

name = argv[1]  # input("name: ")
url = f'http://www.comicsmanics.com/{name}'

s = pq(url=url)

dir = s('h1').text()
try:
    mkdir(dir)
except FileExistsError:
    pass
chdir(dir)

for i, img in enumerate(s('p a')):
    href = pq(img).attr('href')

    imgS = pq(url=href)
    imgUrl = imgS('.pic').attr('src')

    urlretrieve(imgUrl, f'{i:03}.jpg')

    print(f'{imgUrl} > {i:03}.jpg')
