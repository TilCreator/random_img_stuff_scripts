#!/usr/bin/env python3

# Usage: ./inkbunnyPool-dl.py <poolId> <name> <rootFolder> [<passwd>]

import inkbunny
import requests
import os
from sys import argv
from os import mkdir, path

password = None
if len(argv) >= 5:
    password = argv[4]

ib = inkbunny.IB(name='tc_does_stuff', version='0.0', user_nick='TilCreator', username='TilCreator', passwd=password)

pool = ib.search(pool=argv[1], order='pool_order', all=True)

name = argv[2]
posts = pool['submissions']

print(f'Catched: "{name}" ({len(posts)} posts)')

try:
    mkdir(path.join(argv[3], name))
except FileExistsError:
    pass

for i, post in enumerate(posts):
    page_name = f'{i:03}.{post["file_url_full"][post["file_url_full"].rfind(".") + 1:]}'

    if not path.exists(path.join(argv[3], name, page_name)):
        open(path.join(argv[3], name, page_name), 'wb').write(requests.get(post['file_url_full']).content)

        if os.environ.get("SHELL") is not None:
            print(f'Downloaded: "{page_name}" ({i}/{len(posts)})                    ', end='\r')
    else:
        if os.environ.get("SHELL") is not None:
            print(f'Skipping: "{page_name}" ({i}/{len(posts)})                      ', end='\r')

print(f'Downloaded: "{name}" ({len(posts)} posts)                    ')
