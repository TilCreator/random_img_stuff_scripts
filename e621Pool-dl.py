#!/usr/bin/env python3

# Usage: ./e621Pool-dl.py pool_id root_folder

import e621
import requests
import os
import api_keys
from sys import argv
from os import mkdir, path

e = e621.E621(bot_name='tc_does_stuff', version='0.0', user_nick='TilCreator', api_key=api_keys.e621)

pool = e.pool(int(argv[1]))
name = pool['name'].replace('/', '')
post_ids = pool['post_ids']

print(f'Catched: "{name}" ({len(post_ids)} posts)')

try:
    mkdir(path.join(argv[2], name))
except FileExistsError:
    pass

files = os.listdir(path.join(argv[2], name))

for i, post_id in enumerate(post_ids):
    file_matches = [name for name in files if name.startswith(f'{i:03}-{post_id}-')]

    if len(file_matches) == 0:
        post = e.post(post_id)

        page_name = f'{i:03}-{post_id}-{post["file"]["md5"]}.{post["file"]["ext"]}'

        open(path.join(argv[2], name, page_name), 'wb').write(requests.get(post['file']['url']).content)

        if os.environ.get("SHELL") is not None:
            print(f'Downloaded: "{page_name}" ({i}/{len(post_ids)})                    ', end='\r')
    else:
        if os.environ.get("SHELL") is not None:
            print(f'Skipping: "{file_matches[0]}" ({i}/{len(post_ids)})                      ', end='\r')

print(f'Downloaded: "{name}" ({len(post_ids)} posts)                                ')
