#!/usr/bin/env python3

# Usage: cat input | ./filter_for_e621_links.py > e6ids

import sys
import re


id_list = []

for line in sys.stdin:
    for e6id in re.findall(r'e621.net/(?:post/show|posts)/([0-9]*)', line):
        if e6id not in id_list:
            id_list.append(e6id)
            print(e6id)
