import os
import random
import sys
import shutil

images = []
directory = 'tmpRandom'

for dirname, dirs, files in os.walk(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')):
    for file in files:
        file = os.path.join(dirname, file)
        if os.path.isfile(file) and dirname[dirname.rfind('/') + 1:] == '@':
            images.append(file)

print(f'{len(images)} images found')

if not os.path.exists(directory):
    os.mkdir(directory)
else:
    for file in os.listdir(directory):
        os.remove(os.path.join(directory, file))

print(f'Prepared "{directory}"')

for i, file in enumerate(random.sample(images, int(sys.argv[1]))):
    shutil.copyfile(file, os.path.join(directory, f'{str(i).zfill(3)} ({file[file.rfind("html/") + 5:file.rfind("/") - 2].replace("/", ", ").replace("@", "")}){file[file.rfind("."):]}'))

print(f'Copied {int(sys.argv[1])} random images')
