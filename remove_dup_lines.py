#!/usr/bin/env python3

# Usage: cat input | ./remove_dup_lines.py > output

import sys
import re


line_list = []

for line in sys.stdin:
    if line not in line_list:
        line_list.append(line)
        print(line, end='')
