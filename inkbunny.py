import requests
from getpass import getpass


class IB():
    def __init__(self, name='tc_does_stuff', version='0.0', nick='', username='', passwd=None):
        self.username = username
        self.name = name
        self.version = version
        self.nick = nick

        if passwd is None:
            passwd = getpass(f'inkbunny passwd for user "{username}":')

        while True:
            d = self._make_request('login', username=username, password=passwd)

            if 'error_message' in d:
                print(d['error_message'])

                passwd = getpass(f'inkbunny passwd for user "{username}":')
            elif 'sid' in d:
                self.sid = d['sid']
                break

    def _make_request(self, path, **args):
        args.update({'output_mode': 'json'})

        if hasattr(self, 'sid'):
            args.update({'sid': self.sid})

        return requests.post(f'https://inkbunny.net/api_{path}.php', data=args, headers={'User-Agent': f'{self.name}/{self.version} (by {self.nick})'}).json()

    def search(self, tags='', md5=None, pool=None, order=None, limit=100, page=1, all=False):
        if type(tags) is list:
            ' '.join(tags)

        if md5: md5 = 'yes'

        if all:
            data = self._make_request('search', text=tags, submissions_per_page=100, pool_id=pool, md5=md5, orderby=order, get_rid='yes', page=1)
            del data['results_count_thispage']

            for i in range(1, data['pages_count']):
                posts_tmp = self._make_request('search', text=tags, submissions_per_page=100, pool_id=pool, md5=md5, orderby=order, rid=data['rid'], page=i + 1)['submissions']

                for post in posts_tmp:
                    if post not in data['submissions']:
                        data['submissions'].append(post)

            return data
        else:
            return self._make_request('search', text=tags, submissions_per_page=limit, pool_id=pool, md5=md5, orderby=order)

    def submission(self, id=[], pool=True, description=True):
        if isinstance(id, list):
            id = ','.join(id)

        if pool: pool = 'yes'
        else: pool = 'no'
        if description: description = 'yes'
        else: description = 'no'

        return self._make_request('submissions', submission_ids=id, show_pools=pool, show_description=description)
