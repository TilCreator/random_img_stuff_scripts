#!/usr/bin/env python3

# Usage: ./rand_folder.py <path to folder> <only find @ folders (int)> <max amount>
# Creates <folder name>_tmp_random next to the folder

import os
import random
import sys
import shutil

images = []
directory = os.path.join(sys.argv[1], '..', sys.argv[1][sys.argv[1].rfind('/') + 1:] + '_tmp_random')

print(directory)

for dirname, dirs, files in os.walk(sys.argv[1]):
    for file in files:
        file = os.path.join(dirname, file)
        if os.path.isfile(file) and dirname[dirname.rfind('/') + 1:] == '@' or bool(int(sys.argv[2])):
            images.append(file)

print(f'{len(images)} images found')

if not os.path.exists(directory):
    os.mkdir(directory)
#else:
    #for file in os.listdir(directory):
        #os.remove(os.path.join(directory, file))

print(f'Prepared "{directory}"')

amount = int(sys.argv[3])
if amount > len(images) or amount == -1:
    amount = len(images)

for i, file in enumerate(random.sample(images, amount)):
    shutil.copy(file, os.path.join(directory, f'{str(i).zfill(3)} ({file[file.rfind("html/") + 5:].replace("/", ", ").replace("@", "")}){file[file.rfind("."):]}'))

print(f'Copied {amount} random images')
