#!/usr/bin/env python3

# Usage: ./fixExtentions.py
# Works in current dir (recursive)

import magic
import mime
import os

customMimeToExtention = {
    'text/x-python': 'py',
    'image/jpeg': 'jpg'
}

m = magic.Magic(mime=True)

for dirname, dirs, files in os.walk(os.path.dirname(os.path.realpath(__file__))):
    for file in files:
        file = os.path.join(dirname, file)
        if os.path.isfile(file):
            mimeType = m.from_file(file)

            try:
                extentions = mime.Types[mimeType][0].extensions
            except TypeError:
                extentions = ['']

            if mimeType in customMimeToExtention:
                extention = customMimeToExtention[mimeType]
            else:
                extention = extentions[0]

            if extention:
                newFile = f'{file[:file.rfind(".")]}.{extention}'
            else:
                newFile = file

            print(f'{file}\n{mimeType} {extention} ({" ".join(extentions)})\n{file[file.rfind("/") + 1:]} > {newFile[newFile.rfind("/") + 1:]}\n')

            if file != newFile:
                os.rename(file, newFile)
