from urllib.request import urlretrieve
from pyquery import PyQuery as pq
from os import mkdir, chdir, path
from sys import argv


def mkdirF(dir):
    try:
        mkdir(dir)
    except FileExistsError:
        pass


chI = int(argv[1])
url = 'http://www.feretta.net/comic/a-tale-of-tails-1-0/'

chapters = {}
for i, chapter in enumerate([pq(url=url)('#chapter > option')[chI]]):
    print(f'{chI + i}: {pq(chapter).attr("value")}')

    if pq(chapter).attr('value') != '0':
        chapters[pq(chapter).text()] = {}

        for j, page in enumerate(pq(url=pq(url=pq(chapter).attr('value'))('.post-title > a').attr('href'))('.comic-list-dropdown-form > select > option')):
            print(f'{chI + i} {j}: {pq(page).attr("value")}')

            if pq(page).attr('value') != '':
                chapters[pq(chapter).text()][pq(page).text()] = pq(url=pq(page).attr('value'))('#comic img').attr('src')


mkdirF('A Tale of Tails')
chdir('A Tale of Tails')

for i, chapter in enumerate(chapters):
    mkdirF(f'{chI + i:03} - {chapter}')
    for j, img in enumerate(chapters[chapter]):
        urlretrieve(chapters[chapter][img], path.join(f'{chI + i:03} - {chapter}', f'{j:03} - {img}.jpg'))
        print(f'{chapters[chapter][img]} > {j:03} - {img}.jpg')
