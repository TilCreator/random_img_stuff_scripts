#!/usr/bin/env python3

# Usage: cat e6ids | ./e6ids2upvote_remove.py set_id
# Caches actions and doesn't repeat any!

from e621 import E621
import sys
import os

import api_keys


def line_in_file(f, query):
    f.seek(0)
    for line in f.readlines():
        if line.strip('\n') == query:
            return True
    return False


e = E621('yiff collection syncer', version='0.0', user_nick='TilCreator', api_key=api_keys.e621)

with open(f'_scripts/state/e6ids_upvote_remove', 'a+') as ids:
    for line in sys.stdin:
        if os.environ.get("SHELL") is not None:
            print(int(line), end='\r')

        if line_in_file(ids, str(int(line))):
            continue

        r1 = e.vote(id=int(line), score=-1)
        r2 = e.vote(id=int(line), no_unvote=False, score=-1)

        print(int(line), str(r1).strip('\n'), str(r1).strip('\n'))

        if ('success' not in r1.keys() or r1['success'] is True) and ('success' not in r2.keys() or r2['success'] is True):
            ids.seek(0, 2)
            ids.write(str(int(line)) + '\n')
            ids.flush()
