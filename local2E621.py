#!/usr/bin/env python3

# Usage: ./local2E621.py
# Runs in current directory (recursive)
# Writes (/reads) _scripts/state/[hashes|e6ids|none6_paths]

from e621 import E621
from hashlib import md5
import os

import api_keys


e = E621(bot_name='tc_does_stuff', user_nick='TilCreator', api_key=api_keys.e621)

extentions = ['png', 'jpg', 'jpeg', 'gif', 'mp4', 'webm', 'swf']


def line_in_file(f, query):
    f.seek(0)
    for line in f.readlines():
        if line.strip('\n') == query:
            return True
    return False


def terminal_size():
    try:
        import fcntl
        import termios
        import struct

        th, tw, hp, wp = struct.unpack('HHHH', fcntl.ioctl(0, termios.TIOCGWINSZ, struct.pack('HHHH', 0, 0, 0, 0)))
    except (IOError, ModuleNotFoundError):
        th, tw = 80, 200
    return tw, th


def printLine(msg='', noNewLine=False):
    terminalWidth = terminal_size()[0]
    spaces = terminalWidth - len(msg)

    #if os.environ.get("SHELL") is None and not noNewLine:
    #    print(msg)
    #else:
    if noNewLine:
        print(msg + (' ' * spaces), end='\r')
    else:
        print(msg + (' ' * spaces))


with open('_scripts/state/hashes', 'a+') as hashes:
    with open('_scripts/state/e6ids', 'a+') as e6ids:
        with open('_scripts/state/none6_paths', 'a+') as none6_paths:
            for dirname, dirs, files in os.walk(os.path.dirname(os.path.realpath('System'))):
                for file in files:
                    file = os.path.join(dirname, file)
                    if os.path.isfile(file):
                        extention = file[file.rfind('.'):]
                        if 'System/' in file:
                            file_r = file[file.find("System/") + 7:]
                        else:
                            file_r = file

                        if file_r.startswith('.st'):
                            continue

                        if extention[1:] in extentions:
                            hash = md5()
                            with open(file, "rb") as f:
                                for chunk in iter(lambda: f.read(4096), b""):
                                    hash.update(chunk)
                            hash = hash.hexdigest()

                            if os.environ.get("SHELL") is not None:
                                printLine(f'{file_r} {hash}', True)

                            if not line_in_file(hashes, hash):
                                post = e.posts(f'md5:{hash}', 1)

                                hashes.seek(0, 2)
                                hashes.write(hash + '\n')
                                hashes.flush()

                                if post and len(post) > 0:
                                    if not line_in_file(e6ids, post[0]["id"]):
                                        printLine(f'{file_r} {hash} {post[0]["id"]}')

                                        e6ids.seek(0, 2)
                                        e6ids.write(str(post[0]['id']) + '\n')
                                        e6ids.flush()
                                else:
                                    if not line_in_file(none6_paths, file_r):
                                        printLine(f'{file_r} {hash} none6_path')

                                        none6_paths.seek(0, 2)
                                        none6_paths.write(file_r + '\n')
                                        none6_paths.flush()

print('')
