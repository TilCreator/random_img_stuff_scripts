#!/usr/bin/env python3

# Usage: cat paths | ./image2e6.py
# Writes (/reads) _scripts/state/[e6ids|none6_paths_done]

import sys
import os
import re
import time
import json
import logging
from e621 import E621

import api_keys


def line_in_file(f, query, ignore_score=False):
    f.seek(0)
    for line in f.readlines():
        if ignore_score:
            line = line[line.find(':') + 1:]
        if line.strip('\n') == query:
            return True
    return False


logger = logging.getLogger('image2e6')
if not os.environ.get("SHELL") is None:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.ERROR)

e = E621(bot_name='tc_does_stuff', user_nick='TilCreator', api_key=api_keys.e621)

with open('_scripts/state/e6ids', 'a+') as e6ids:
    with open('_scripts/state/none6_paths_done', 'a+') as none6_paths_done:
        for line in sys.stdin:
            if os.environ.get("SHELL") is not None:
                print(line.strip('\n'), end='\r')

            if not line_in_file(none6_paths_done, line.strip('\n'), True) and line.strip('\n')[line.strip('\n').rfind('.') + 1:] in ['gif', 'jpg', 'jpeg', 'png', 'bmp', 'svg', 'webp']:
                if not os.path.exists(line.strip('\n')):
                    logger.warning('FileNotFoundError: ' + line.strip('\n'))
                    continue

                results = e.iqdb(file=open(line.strip('\n'), 'rb'))

                out_print = line.strip('\n') + ' '

                score = 0

                if not results is None and type(results) == list and len(results) > 0:
                    for result in results:
                        if not line_in_file(e6ids, str(result['post_id'])):
                            e6ids.seek(0, 2)
                            e6ids.write(str(result['post_id']) + '\n')
                            e6ids.flush()

                        out_print += str(result['post_id']) + ' '
                        score = max(score, result['score'])

                print(out_print)

                none6_paths_done.seek(0, 2)
                none6_paths_done.write(str(score) + ':' + line.strip('\n') + '\n')
                none6_paths_done.flush()
