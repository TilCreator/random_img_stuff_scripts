#!/usr/bin/sh
trap "exit" INT

unset SHELL  # Disables progress report

echo \# Dump telegram yiff
python _scripts/telegram_chat_exporter.py "[NSFW] E621 inline" telegram/files >> telegram/msg_text

while true; do
    echo \# Updating pools
    if _scripts/update_pools.sh; then
        break
    fi
done

while true; do
    echo \# Scan local dir
    if python _scripts/local2E621.py; then
        break
    fi
done

echo \# Filter chats for e6 links
cat telegram/msg_text | python _scripts/filter_for_e621_links.py >> _scripts/state/e6ids

echo \# searching for similar images on e621
cat _scripts/state/none6_paths | python _scripts/image2e6.py

#while true; do
#    echo \# Saucenaoing images
#    if cat _scripts/state/none6_paths | python _scripts/saucenau2e6.py; then
#        break
#    fi
#
#    echo \# Saucenaoing images \(tor\)
#    if cat _scripts/state/none6_paths | torsocks python _scripts/saucenau2e6.py jup; then
#        break
#    fi
#
#    echo \# Restarting tor
#    sudo systemctl restart tor
#done

# TODO get favs and sets

echo \# Removing dupplicate e6ids
cp _scripts/state/e6ids /tmp
cat /tmp/e6ids | python _scripts/remove_dup_lines.py > _scripts/state/e6ids

exit # TODO remove

while true; do
    echo \# Add e6ids to set
    #if cat _scripts/state/e6ids | python _scripts/e6ids2set.py 19230; then break; fi
    #if cat _scripts/state/e6ids | python _scripts/e6ids2fav.py; then break; fi
    if cat _scripts/state/e6ids | python _scripts/e6ids2upvote.py; then break; fi
done

while true; do
    echo \# Removing blacklist from set
    #if python _scripts/e6set2ids.py 19355 | python _scripts/e6ids_remove_set.py 19230; then break; fi
    #if python _scripts/e6set2ids.py 19355 | python _scripts/e6ids2favs_remove.py; then break; fi # TODO
    if python _scripts/e6set2ids.py 19355 | python _scripts/e6ids2upvote_remove.py; then break; fi
done

echo \# Echo state
_scripts/state.sh
